from rest_framework.viewsets import ModelViewSet
from rest_framework.authentication import BasicAuthentication
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import IsAuthenticated
from .serialisers import JobSerialiser


class JobViewSet(ModelViewSet):
    serializer_class = JobSerialiser
    permission_classes = (IsAuthenticated,)
    authentication_classes = [JSONWebTokenAuthentication, BasicAuthentication]

    def get_queryset(self):
        return self.request.user.applications.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
