from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework_jwt.settings import api_settings
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
)
from .serialisers import JobSerialiser
from .models import Job, APPLICATION_STATUS


def get_jwt(user):
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token


def deserialiseJob(data, many=False):
    serialiser = JobSerialiser(data=data, many=many)
    assert serialiser.is_valid()
    return serialiser.save()


class TrackerTests(APITestCase):
    def login(self, user='test_user'):
        user = User.objects.get_or_create(username=user)[0]
        self.client.force_authenticate(user)
        self.client.credentials(Authorization='JWT ' + get_jwt(user))
        return user

    def test_add_job_succeeds(self):
        """
        Job can be added successfully
        """
        self.login()
        url = reverse('jobs-list')
        data = {'position_title': 'test job'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(Job.objects.count(), 1)
        self.assertEqual(Job.objects.get().position_title, 'test job')

    def test_get_status_options(self):
        """
        Get available job status options
        """
        self.login()
        url = reverse('jobs-list')
        response = self.client.options(url)
        self.assertEqual(response.status_code, HTTP_200_OK)
        choices = response.data['actions']['POST']['status']['choices']
        self.assertEqual(len(choices), len(APPLICATION_STATUS))

    def test_fetch_all(self):
        """
        Get all jobs
        """
        user = self.login()
        job1 = Job(position_title='dummy job 1', user=user)
        job2 = Job(position_title='dummy job 2', user=user)
        job1.save()
        job2.save()
        url = reverse('jobs-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTP_200_OK)
        entities = deserialiseJob(response.data, many=True)
        self.assertEqual(len(entities), 2)

    def test_fetch_one_job(self):
        """
        Can fetch a single job by id
        """
        user = self.login()
        job = Job(position_title='another test job', user=user)
        job.save()
        url = reverse('jobs-detail', kwargs={'pk': job.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTP_200_OK)
        new_job = deserialiseJob(response.data)
        self.assertEqual(new_job.position_title, 'another test job')
        self.assertEqual(new_job.user, user)

    def test_delete_job(self):
        """
        Can delete a job by id
        """
        user = self.login()
        job = Job(position_title='test position', user=user)
        job.save()
        url = reverse('jobs-detail', kwargs={'pk': job.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTP_200_OK)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTP_404_NOT_FOUND)

    def test_update_job(self):
        """
        Update a job
        """
        user = self.login()
        job = Job(position_title='test position', user=user)
        job.save()
        url = reverse('jobs-detail', kwargs={'pk': job.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTP_200_OK)
        job = deserialiseJob(response.data)
        job.position_title = 'updated position'
        serialiser = JobSerialiser(job)
        response = self.client.put(url, serialiser.data)
        self.assertEqual(response.status_code, HTTP_200_OK)
        updated = deserialiseJob(response.data)
        self.assertEqual(updated.position_title, 'updated position')


class AuthAndPermTests(APITestCase):
    def test_incorrect_auth_fails(self):
        """
        Authenticate with incorrect credentials fails
        """
        url = reverse('jwt_login')
        data = {
          'username': 'user',
          'password': 'inCorrectPassword1'
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)
        error = response.data['non_field_errors'][0]
        self.assertIn('Unable to log in with provided credentials', error)

    def test_authenticate(self):
        """
        Authenticate with correct credentials
        """
        User.objects.create_user(username='test_user',
                                 password='correctPassword1')
        url = reverse('jwt_login')
        data = {
            'username': 'test_user',
            'password': 'correctPassword1'
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertIsNotNone(response.data['token'])

    def test_token_refresh(self):
        """
        Token refresh succeeds
        """
        correct_user = User.objects.create_user(username='test_user',
                                                password='correctPassword1')
        token = get_jwt(correct_user)
        url = reverse('jwt_refresh')
        data = {
            'token': token
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertIsNotNone(response.data['token'])

    def test_jobs_filtered_by_user(self):
        """
        Job-details method only returns jobs created by user
        """
        user1 = User.objects.create_user(username='user1', password='user1Password')
        user2 = User.objects.create_user(username='user2', password='user2Password')
        job1 = Job(position_title="test job user 1", user=user1)
        job2 = Job(position_title="test job user 2", user=user2)
        job1.save()
        job2.save()
        url = reverse('jobs-list')
        self.client.force_authenticate(user1)
        self.client.credentials(Authorization='JWT ' + get_jwt(user1))
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTP_200_OK)
        jobs = deserialiseJob(response.data, many=True)
        self.assertEqual(len(jobs), 1)
        self.assertEqual(jobs[0].position_title, 'test job user 1')
        self.assertEqual(jobs[0].user, user1)

    def test_job_detail_for_wrong_user_404(self):
        """
        Attempting to return a job for incorrect user returns 404
        """
        user = User.objects.create_user(username='user1', password='user1Password')
        wrong_user = User.objects.create_user(username='wrong', password='user2Password')
        job = Job(position_title="test job", user=user)
        job.save()
        url = reverse('jobs-detail', kwargs={'pk': job.id})
        self.client.force_authenticate(wrong_user)
        self.client.credentials(Authorization='JWT ' + get_jwt(wrong_user))
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTP_404_NOT_FOUND)
