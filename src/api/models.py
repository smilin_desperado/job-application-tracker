from django.db import models
from django.contrib.auth.models import User

APPLICATION_STATUS = (
    ("1", 'Researching'),
    ("2", 'Applied'),
    ("3", 'Interviewed For'),
    ("4", 'Offer Made'),
    ("5", 'Rejected'),
)


class Job(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name="applications", null=True)
    position_title = models.CharField(max_length=100)
    company = models.CharField(max_length=100, blank=True)
    salary = models.IntegerField(blank=True, null=True)
    url = models.URLField(max_length=1000, blank=True)
    contact_person = models.CharField(max_length=100, blank=True)
    contact_email = models.EmailField(blank=True)
    phone_number = models.IntegerField(blank=True, null=True)
    application_data = models.TextField(blank=True)
    status = models.CharField(max_length=50, choices=APPLICATION_STATUS, default='1')
    date_applied = models.DateField(null=True, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.position_title

    class Meta:
        ordering = ['status', ]


class DataPoint(models.Model):
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    change_to = models.CharField(max_length=50, choices=APPLICATION_STATUS, default='1')
    change_date = models.DateTimeField(auto_now_add=True)
