from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Job, DataPoint


@receiver(post_save, sender=Job)
def job_handler(sender, instance, **kwargs):
    try:
        dp = DataPoint.objects.filter(job=instance.id).order_by('-change_date')[0]
        if dp.change_to != instance.status:
            dp = DataPoint()
            dp.job = instance
            dp.change_to = instance.status
            dp.save()
    except IndexError:
        dp = DataPoint()
        dp.job = instance
        dp.change_to = instance.status
        dp.save()
