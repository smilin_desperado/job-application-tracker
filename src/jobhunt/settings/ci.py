from .base import *

DEBUG = True

SECRET_KEY = '8rmsw_q+1s^_bs$eavju8nfe6a+k1540zhhy2e*&0cukh^9j*5'
INTERNAL_IPS = ['127.0.0.1']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'ci',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}
