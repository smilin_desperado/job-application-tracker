import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import Logout from '@/components/Logout'
import Vuex from 'vuex'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('Logout.vue', () => {
  let store
  let actions
  let stubs

  beforeEach (() => {
    actions = {
      logout: jest.fn()
    }
    store = new Vuex.Store({
      state: {},
      actions
    })
    stubs = {
      RouterLink: RouterLinkStub
    }
  })

  it('should display the correct contents', () => {
    const wrapper = shallowMount(Logout, {store, localVue, stubs})
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('should dispatch logout', () => {
    const wrapper = shallowMount(Logout, {store, localVue, stubs})    
    expect(actions.logout).toHaveBeenCalled()
  })
})

