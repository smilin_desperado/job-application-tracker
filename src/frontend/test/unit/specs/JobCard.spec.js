import { shallowMount } from '@vue/test-utils'
import JobCard from '@/components/JobCard'

describe('JobCard.vue', () => {
  const wrapper = shallowMount(JobCard, {
    propsData: {
      title: 'test title',
      company: 'test company',
      date_modified: new Date('01-01-2010')
    }
  })

  it('should display correct contents', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('should emit open-job', () => {
    wrapper.find('.job-card').trigger('click')
    expect(wrapper.emitted('open-job')).toBeTruthy()
  })
})
