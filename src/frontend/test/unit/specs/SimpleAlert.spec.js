import { shallowMount } from '@vue/test-utils'
import SimpleAlert from '@/components/SimpleAlert'

describe('SimpleAlert.vue', () => {
  it('should display the correct contents', () => {
    const wrapper = shallowMount(SimpleAlert, {
      propsData: {
        message: 'test message'
      }
    })
    expect(wrapper.html()).toMatchSnapshot()
  })
})
