import { shallowMount, createLocalVue } from '@vue/test-utils'
import Listing from '@/components/Listing'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Listing.vue', () => {
  let actions
  let store
  let getters

  beforeEach(() => {
    actions = {
      notify: jest.fn(),
      setStatusOptions: jest.fn()
    } 
    getters = {
      apiConfig: () => {}
    }
    store = new Vuex.Store({
      state: {
        statusOptions: [
          { display_name: 'groupA', value: '1' },
          { display_name: 'groupB', value: '2' }
        ]},
      getters,
      actions 
    })
  })

  it('should set status options on created', () => {
    const wrapper = shallowMount(Listing, {store, localVue})
    expect(actions.setStatusOptions).toHaveBeenCalled()
  })
  
  it('should display base contents when no jobs', () => {
    const wrapper = shallowMount(Listing, {store, localVue})  
    expect(wrapper.html()).toMatchSnapshot()
  })
  
  it.skip('should group jobs by status title correctly', () =>{
    const wrapper = shallowMount(Listing, {store, localVue})
    const data = [
      { position_title: 'job 1', status: '1' },
      { position_title: 'job 2', status: '2' },
      { position_title: 'job 3', status: '1' }
    ]
    wrapper.setData({jobs: data})
    expect(wrapper.vm.groupedJobs.groupA).toBeDefined()
  })
})
