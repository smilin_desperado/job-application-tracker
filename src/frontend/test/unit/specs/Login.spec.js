import { shallowMount } from '@vue/test-utils'
import Login from '@/components/Login'

describe('Login.vue', () => {
  it('should display correct contents', () => {
    const wrapper = shallowMount(Login)
    expect(wrapper.html()).toMatchSnapshot()
  })
})
