import Vue from 'vue'
import VueRouter from 'vue-router'
import Listing from '@/components/Listing.vue'
import JobView from '@/components/JobView.vue'
import Login from '@/components/Login.vue'
import Logout from '@/components/Logout.vue'
import store from '@/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'main',
    component: Listing,
    meta: { requiresAuth: true }
  },
  {
    path: '/job/:id',
    name: 'job',
    component: JobView,
    meta: { requiresAuth: true }
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout
  }
]

const router = new VueRouter({
  mode: 'history',
  routes: routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    store.dispatch('checkAndRefreshToken')
      .then(() => next())
      .catch(() => next({name: 'login'}))
  } else {
    next()
  }
})

export { router as default }
