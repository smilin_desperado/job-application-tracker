import Vue from 'vue'
import Vuex from 'vuex'
import client from '../client'
import jwtDecode from 'jwt-decode'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  state: {
    jwt: localStorage.getItem('t'),
    refreshDelay: 10 * 60,
    notificationTimeout: 5 * 1000,
    statusOptions: null,
    notifications: []
  },
  getters: {
    username: (state) => state.jwt ? jwtDecode(state.jwt).username : '',
    apiConfig: (state) => {
      return {
        baseURL: 'http://localhost:8000/api/jobs/',
        timeout: 5000,
        headers: {
          Authorization: `JWT ${state.jwt}`
        }
      }
    }
  },
  mutations: {
    API_FAILURE (state, payload) {
      console.log(payload)
      state.errors = payload
    },
    UPDATE_TOKEN (state, newToken) {
      state.jwt = newToken
      localStorage.setItem('t', newToken)
    },
    DELETE_TOKEN (state) {
      localStorage.removeItem('t')
      state.jwt = null
    },
    SET_STATUS_OPTIONS (state, options) {
      state.statusOptions = options
    },
    NOTIFY (state, message) {
      state.notifications.push(message)
    },
    REMOVE_NOTIFICATION (state, message) {
      state.notifications.splice(state.notifications.indexOf(message), 1)
    }
  },
  actions: {
    getToken ({commit, state}, {user, pass}) {
      return new Promise((resolve, reject) => {
        client.login(user, pass)
          .then(data => {
            commit('UPDATE_TOKEN', data)
            resolve()
          }).catch(error => reject(error))
      })
    },

    checkAndRefreshToken ({commit, state, dispatch}) {
      return new Promise((resolve, reject) => {
        let expiry = jwtDecode(state.jwt).exp
        let nowSeconds = Math.ceil(Date.now() / 1000)
        if (nowSeconds > expiry) {
          commit('DELETE_TOKEN')
          dispatch('notify', {text: 'You have been logged out, please log in', alert: 'warning'})
          reject(new Error('Login has expired'))
        } else if (nowSeconds > expiry - state.refreshDelay) {
          client.refreshToken(state.jwt)
            .then(data => {
              commit('UPDATE_TOKEN', data)
              resolve()
            }).catch(error => reject(error))
        } else {
          resolve()
        }
      })
    },

    setStatusOptions ({commit, state, getters}) {
      if (state.statusOptions === null) {
        client.getStatusOptions(getters.apiConfig)
          .then(data => commit('SET_STATUS_OPTIONS', data))
          .catch(error => console.log(error))
      }
    },

    notify ({commit, state}, message) {
      commit('NOTIFY', message)
      window.setTimeout(commit, state.notificationTimeout, 'REMOVE_NOTIFICATION', message)
    },

    logout ({commit}) {
      commit('DELETE_TOKEN')
    }
  }
})
