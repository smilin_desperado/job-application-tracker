import axios from 'axios'

export default {
  fetchAll (config) {
    return axios.get('/', config)
      .then(response => Promise.resolve(response.data))
      .catch(error => Promise.reject(error))
  },

  fetchOne (id, config) {
    return axios.get(`${id}/`, config)
      .then(response => Promise.resolve(response.data))
      .catch(error => Promise.reject(error))
  },

  addJob (job, config) {
    const payload = { position_title: job }
    return axios.post('', payload, config)
      .then(response => Promise.resolve(response.data))
      .catch(error => Promise.reject(error))
  },

  updateJob (job, config) {
    return axios.put(`${job.id}/`, job, config)
      .then(response => Promise.resolve(response.data))
      .catch(error => Promise.reject(error))
  },

  getStatusOptions (config) {
    return axios.options('', config)
      .then(response => Promise.resolve(response.data.actions.POST.status.choices))
      .catch(error => Promise.reject(error))
  },

  deleteJob (id, config) {
    return axios.delete(`${id}/`, config)
      .then(response => Promise.resolve(response.data))
      .catch(error => Promise.reject(error))
  },

  login (user, pass) {
    return axios.post('http://localhost:8000/api-token-auth/', {username: user, password: pass})
      .then(response => Promise.resolve(response.data.token))
      .catch(error => Promise.reject(error))
  },

  refreshToken (token) {
    return axios.post('http://localhost:8000/api-token-refresh/', {token: token})
      .then(response => Promise.resolve(response.data.token))
      .catch(error => Promise.reject(error))
  }
}
