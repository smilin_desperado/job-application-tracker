# Job Application Tracker 
[![pipeline status](https://gitlab.com/smilin_desperado/job-application-tracker/badges/master/pipeline.svg)](https://gitlab.com/smilin_desperado/job-application-tracker/commits/master)

## What is it?
A web application to help track details about your job hunting efforts e.g. potential job details, contact info, application status etc.


